Feature: Create a voting
  As a facilitator
  I want to create a voting
  So participants can vote

  Scenario: Create empty voting without participants neither alternatives
    When I create a voting with name "Choose your books" with 3 votes per participant
    Then the voting was successfully created
    And I'm the facilitator of this voting

  Scenario: Create voting with alternatives and without participants
    When I create a voting with name "Choose your books" with 3 votes per participant and alternatives "A", "B", "C"
    Then the voting was successfully created
    And I'm the facilitator of this voting
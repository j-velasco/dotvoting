module.exports = function () {
  this.Given(/^the user with email "([^"]*)" is registered$/, function (anEmail, callback) {
    this.registerUserService.registerUserWithEmail(anEmail)
    callback()
  })
}

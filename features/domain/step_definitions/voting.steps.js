module.exports = function () {
  let expectedVotingName
  let expectedMaxVotes
  let expectedAlternatives
  let createdVotingId
  let aNonFacilitatorUserId = 'no-facilitator-1234'
  let exceptionOnVote

  this.When(/^I create a voting with name "([^"]*)" with (\d+) votes per participant$/,
    function (aName, someNumberOfVotes, callback) {
      expectedVotingName = aName
      expectedMaxVotes = someNumberOfVotes
      createdVotingId = this.createVotingService.createVoting(
        this.currentUserId,
        aName,
        someNumberOfVotes
      )
      expectedAlternatives = []
      callback()
    }
  )

  this.When(/^I created? a voting with name "([^"]*)" with (\d+) votes per participant and alternatives (.*)$/,
    function (aName, someNumberOfVotes, alternativesList, callback) {
      const alternatives = alternativesList.split(',').map((s) => s.trim().replace(/"/g, ''))
      expectedVotingName = aName
      expectedMaxVotes = someNumberOfVotes
      expectedAlternatives = alternatives
      createdVotingId = this.createVotingService.createVoting(
        this.currentUserId,
        aName,
        someNumberOfVotes,
        alternatives
      )
      callback()
    }
  )

  this.Then(/^the voting was successfully created$/, function (callback) {
    const voting = this.getVotingServiceById.votingWithId(createdVotingId)

    assertThat(voting, is(object()))
    assertThat(voting.name, equalTo(expectedVotingName))
    assertThat(voting.maxVotes, equalTo(expectedMaxVotes))

    voting.alternatives.forEach((alternative, idx) => {
      assertThat(alternative, equalTo(expectedAlternatives[idx]))
    })

    callback()
  })

  this.Then(/^I'm the facilitator of this voting$/, function (callback) {
    const voting = this.getVotingServiceById.votingWithId(createdVotingId)

    assertThat(voting.facilitatorId, equalTo(this.currentUserId))
    callback()
  })

  this.When(
    /^an?( non)? invited user votes with "([^"]*)" dots for alternative "([^"]*)" and "([^"]*)" dot for alternative "([^"]*)"$/,
    function (non, dotsForFirstAlternative, firstAlternative, dotsForSecondAlternative, secondAlternative, callback) {
      const isInvited = non === undefined
      if (isInvited) {
        this.inviteUserToVotingService(createdVotingId, aNonFacilitatorUserId)
      }
      const dotsPerAlternative = [
        {
          alternative: firstAlternative,
          dots: dotsForFirstAlternative
        },
        {
          alternative: secondAlternative,
          dots: dotsForSecondAlternative
        }
      ]
      try {
        this.voteInVoting(createdVotingId, aNonFacilitatorUserId, dotsPerAlternative)
      } catch (ex) {
        exceptionOnVote = ex
      }
      callback()
    })

  this.Then(/^the vote is not permitted$/, function (callback) {
    assertThat(exceptionOnVote, userNotAllowedToVoteOnVoting(createdVotingId, aNonFacilitatorUserId))
    callback()
  });

  this.Then(/^the current voting result is:$/, function (resultTable, callback) {
    const expectedResult = resultTable
      .rows()
      .map((aRow, idx) => {
        return {
          position: idx + 1,
          alternative: aRow[0],
          dots: parseInt(aRow[1])
        }
      })
    const votingResult = this.getVotingResult(createdVotingId)
    assertThat(votingResult, equivalentArray(expectedResult))
    callback()
  })
}

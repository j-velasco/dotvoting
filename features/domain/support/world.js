import VotingRepository from 'Ports/Persistance/InMemory/VotingRepository'
import UserRepository from 'Ports/Persistance/InMemory/UserRepository'

import CreateVotingService from 'Application/CreateVotingService'
import RegisterUserService from 'Application/RegisterUserService'
import GetVotingServiceById from 'Application/GetVotingServiceById'
import { factory as getVotingResultFactory } from '../../../src/Application/Service/getVotingResult'
import { factory as inviteUserToVotingFactory } from '../../../src/Application/Service/inviteUserToVoting'
import { factory as voteInVotingServiceFactory } from '../../../src/Application/Service/voteInVoting'

function World () {
  this.votingRepo = new VotingRepository()
  this.userRepo = new UserRepository()

  this.createVotingService = new CreateVotingService(this.votingRepo)
  this.registerUserService = new RegisterUserService(this.userRepo)
  this.getVotingServiceById = new GetVotingServiceById(this.votingRepo)
  this.voteInVoting = voteInVotingServiceFactory(this.votingRepo)
  this.getVotingResult = getVotingResultFactory(this.votingRepo)
  this.inviteUserToVotingService = inviteUserToVotingFactory(this.votingRepo, this.userRepo)

  this.currentUserId = this.registerUserService.registerUserWithEmail('facilitator@example.com')
}

module.exports = function () {
  this.World = World
}

Feature: Invite participants to a voting
  As a facilitator
  I want to invite participants
  So they can vote in this voting

  Background:
    Given I created a voting with name "Choose your books" with 3 votes per participant and alternatives "A", "B", "C"

  Scenario: A non invited user votes
    When a non invited user votes with "2" dots for alternative "A" and "1" dot for alternative "B"
    Then the vote is not permitted

  Scenario: An invited user votes
    When an invited user votes with "2" dots for alternative "A" and "1" dot for alternative "B"
    Then the current voting result is:
      | alternative | total_dots |
      | A           | 2          |
      | B           | 1          |
      | C           | 0          |
import User from 'Domain/Model/User'
import UserId from 'Domain/Model/UserId'
import Email from 'Domain/Model/Email'

class RegisterUserService {
  constructor (userRepository) {
    this.userRepository = userRepository
  }

  registerUserWithEmail (anEmail) {
    const id = new UserId()
    const email = new Email(anEmail)

    var user = new User(id, email)
    this.userRepository.save(user)

    return user.id.id
  }
}

export default RegisterUserService

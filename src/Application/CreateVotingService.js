import Voting from 'Domain/Model/Voting'
import UserId from '../Domain/Model/UserId'

class CreateVotingService {
  constructor (votingRepository) {
    this.votingRepository = votingRepository
  }

  createVoting (aFacilitatorId, aName, someNumberOfVotes, alternatives = []) {
    const id = this.votingRepository.nextIdentity()
    const facilitatorId = new UserId(aFacilitatorId)
    var voting = new Voting(id, facilitatorId, aName, someNumberOfVotes, alternatives)

    this.votingRepository.save(voting)

    return id.id
  }
}

export default CreateVotingService

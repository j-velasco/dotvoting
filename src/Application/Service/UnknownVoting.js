function UnknownVoting (votingId) {
  this.message = `Voting of id ${votingId} doesn't exists`
}

export default UnknownVoting

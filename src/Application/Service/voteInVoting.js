import VotingId from '../../Domain/Model/VotingId'
import UserId from '../../Domain/Model/UserId'
import UnknownVoting from './UnknownVoting'

const factory = (votingRepository) => {
  return (aVotingId, aVoterId, aDotsPerAlternativeList) => {
    const voting = votingRepository.votingOfId(new VotingId(aVotingId))

    if (!voting) {
      throw new UnknownVoting(new VotingId(aVotingId))
    }
    const voterId = new UserId(aVoterId)
    voting.addVote(voterId, aDotsPerAlternativeList)
  }
}

export { factory }

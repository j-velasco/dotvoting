function UserNotAllowedToVote (votingId, userId) {
  this.message = `User with id ${userId} is not allowed to vote on voting with id ${votingId}`
}

export default UserNotAllowedToVote


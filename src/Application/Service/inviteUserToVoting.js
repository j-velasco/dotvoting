import VotingId from 'Domain/Model/VotingId'
import UserId from 'Domain/Model/UserId'
import UnknownVoting from "./UnknownVoting";

const factory = (aVotingRepository) => {
  return (aVotingId, aUserId) => {
    const voting = aVotingRepository.votingOfId(new VotingId(aVotingId))

    if (!voting) {
      throw new UnknownVoting(aVotingId)
    }

    voting.addVoter(new UserId(aUserId))
  }
}

export { factory }

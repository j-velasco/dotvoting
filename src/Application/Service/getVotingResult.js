import VotingId from '../../Domain/Model/VotingId'
import UnknownVoting from './UnknownVoting';

const factory = (votingRepository) => {
  return (aVotingId) => {
    const votingId = new VotingId(aVotingId)
    const voting = votingRepository.votingOfId(votingId)
    if (!voting) {
      throw new UnknownVoting(votingId)
    }

    return voting.result().dto()
  }
}

export { factory }
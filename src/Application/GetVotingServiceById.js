import VotingId from 'Domain/Model/VotingId'

class GetVotingServiceById {
  constructor (aVotingRepository) {

    this.votingRepository = aVotingRepository
  }

  votingWithId (aVotingId) {
    const id = new VotingId(aVotingId)

    const voting = this.votingRepository.votingOfId(id)

    return voting.dto()
  }
}

export default GetVotingServiceById

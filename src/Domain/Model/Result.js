class Result {
  constructor (aListOfAlternatives, aVotes) {
    this.alternatives = aListOfAlternatives
    this.votes = aVotes
  }

  dto () {
    const skeleton = this.createSkeleton()

    this.votesPerVoter()
      .forEach((dotsPerAlternative) => {
        dotsPerAlternative.forEach((dots, alternative) => {
          skeleton.set(
            alternative.value,
            skeleton.get(alternative.value) + parseInt(dots)
          )
        })
      })

    const consolidatedDots = []
    skeleton
      .forEach((dots, alternative) => {
        consolidatedDots.push({alternative, dots})
      })

    return consolidatedDots
      .sort((a, b) => a.dots < b.dots ? 1 : 0)
      .reduce((acc, pair, idx) => {
        const prev = acc.length > 0 ? acc[acc.length - 1] : null
        const pos = prev && prev.dots === pair.dots ? prev.position : idx + 1
        return [
          ...acc,
          Object.assign({}, pair, {position: pos})
        ]
      }, [])
  }

  votesPerVoter () {
    const votesPerVoter = new Map()
    this.votes
      .forEach((vote) => {
        votesPerVoter.set(vote.voterId, vote.dotsPerAlternative)
      })
    return votesPerVoter
  }

  createSkeleton () {
    const skeleton = new Map()
    this.alternatives.forEach((alternative) => {
      skeleton.set(alternative.value, 0)
    })
    return skeleton
  }
}

export default Result

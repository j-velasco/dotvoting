import UserId from './UserId'
import Email from './Email'

class User {
  constructor (anId, anEmail) {
    this.email = anEmail
    this.id = anId
  }

  static fromDto ({ id, email }) {
    return new User(new UserId(id), new Email(email))
  }

  get dto () {
    return {
      id: '' + this.id,
      email: '' + this.email
    }
  }
}

export default User

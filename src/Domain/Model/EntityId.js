import uuid from 'uuid'
import areOfTheSameType from './areOfTheSameType'

class EntityId {
  constructor (anId) {
    this.id = anId || uuid.v4()
  }

  equalsTo (otherId) {
    return areOfTheSameType(otherId, this) && otherId.id === this.id
  }

  toString () {
    return this.id
  }
}

export default EntityId

import VotingId from './VotingId'
import Alternative from './Alternative'
import UserId from './UserId'
import Vote from './Vote'
import Result from './Result'
import UserNotAllowedToVote from "../../Application/Service/UserNotAllowedToVote";

class Voting {
  constructor (anId, aFacilitatorId, aName, someNumberOfVotes, someAlternatives) {
    this.id = anId
    this.facilitatorId = aFacilitatorId
    this.name = aName
    this.maxVotes = someNumberOfVotes
    this.alternatives = new Map()
    someAlternatives.forEach((anAlternative) => this.addAlternative(anAlternative))
    this.votes = []
    this.voters = []
  }

  static fromDto (dto) {
    return new Voting(
      new VotingId(dto.id),
      new UserId(dto.facilitatorId),
      dto.name,
      dto.maxVotes,
      dto.alternatives
    )
  }

  addAlternative (anAlternative) {
    const alternative = new Alternative(anAlternative)
    this.alternatives.set(alternative.value, alternative)
  }

  addVote (aVoterId, aListOfDotsPerAlternative) {
    if (!this.isInVoterList(aVoterId)) {
      throw new UserNotAllowedToVote(this.id, aVoterId)
    }

    const totalDots = aListOfDotsPerAlternative.reduce((acc, pair) => acc + pair.dots, 0)
    if (totalDots > this.maxVotes) {
      throw new Error(`Max dots allowed exceeded: ${this.maxVotes} and ${totalDots} passed`)
    }

    const listOfDotsPerAlternative = aListOfDotsPerAlternative.map((pair) => {
      if (!this.alternatives.has(pair.alternative)) {
        throw new Error(`Alternative "${pair.alternative} does exists in this voting`)
      }

      return {
        alternative: this.alternatives.get(pair.alternative),
        dots: pair.dots
      }
    })

    this.votes.push(new Vote(this.id, aVoterId, listOfDotsPerAlternative))
  }

  addVoter (aUserId) {
    this.voters.push(aUserId)
  }

  result () {
    return new Result(this.alternatives, this.votes)
  }

  dto () {
    const alternatives = []
    this.alternatives.forEach((alt) => {
      alternatives.push({
        value: alt.value
      })
    })
    const votes = []
    this.votes.forEach((vote) => {
      votes.push(vote.dto)
    })

    return {
      id: this.id.id,
      facilitatorId: this.facilitatorId.id,
      name: this.name,
      maxVotes: this.maxVotes,
      alternatives: alternatives.map((a) => a.value),
      votes
    }
  }

  isInVoterList(aVoterId) {
    return this.voters.filter((voter) => voter.equalsTo(aVoterId)).length === 1
  }
}

export default Voting

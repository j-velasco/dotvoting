class Vote {
  constructor (aVotingId, aVoterId, aListOfDotsPerAlternative) {
    this.votingId = aVotingId
    this.voterId = aVoterId
    this.dotsPerAlternative = new Map()
    aListOfDotsPerAlternative.forEach((dotPerAlternative) => {
      this.dotsPerAlternative.set(dotPerAlternative.alternative, dotPerAlternative.dots)
    })
  }

  get dto () {
    const dotsPerAlternative = []
    this.dotsPerAlternative.forEach((dots, alternative) => {
      dotsPerAlternative.push({
        alternative: '' + alternative,
        dots
      })
    })
    return {
      voterId: '' + this.voterId,
      dotsPerAlternative
    }
  }
}

export default Vote

import areOfTheSameType from './areOfTheSameType'

class Email {
  constructor (anEmail) {
    this.email = anEmail
  }

  equalsTo (otherEmail) {
    return areOfTheSameType(otherEmail, this) && this.email === otherEmail.email
  }

  toString () {
    return this.email
  }
}

export default Email

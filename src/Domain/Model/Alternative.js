class Alternative {
  constructor (aValue) {
    this.value = aValue
  }

  toString () {
    return this.value
  }
}

export default Alternative

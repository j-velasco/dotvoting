export default (anObject, otherObject) => anObject.constructor.name === otherObject.constructor.name

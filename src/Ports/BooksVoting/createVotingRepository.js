import Voting from 'Domain/Model/Voting'
import UserId from 'Domain/Model/UserId'
import VotingRepository from 'Ports/Persistance/InMemory/VotingRepository'

export default (db) => {
  const votingRepository = new VotingRepository()

  db.select().from('aggregate').where('type', 'Domain/Model/Voting').rows((err, rows) => {
    if (err) {
      console.error('error reading voting', err)
      return
    }
    rows.forEach(({ data }) => {
      const voting = Voting.fromDto(data)
      data.votes.forEach(({ voterId, dotsPerAlternative }) => {
        voting.addVote(new UserId(voterId), dotsPerAlternative)
      })
      votingRepository.save(voting)
    })
  })

  return votingRepository
}

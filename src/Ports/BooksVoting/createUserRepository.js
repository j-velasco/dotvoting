import User from 'Domain/Model/User'
import UserRepository from 'Ports/Persistance/InMemory/UserRepository'

export default (db) => {
  const usersRepository = new UserRepository()

  db.select().from('aggregate').where('type', 'Domain/Model/User').rows((err, rows) => {
    if (err) {
      console.error('error reading users', err)
      return
    }
    rows.forEach((row) => {
      usersRepository.save(User.fromDto(row.data))
    })
  })

  return usersRepository
}

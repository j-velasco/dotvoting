module.exports = [
  {
    'field_id': '21265051',
    'title': 'Practical Object-Oriented Design in Ruby: An Agile Primer (Addison-Wesley Professional Ruby) 1st Edition',
    'url': 'http://www.amazon.com/Practical-Object-Oriented-Design-Ruby-Addison-Wesley/dp/0321721330',
    'the_proposer_have_read_it': false,
    'description_from_proposer': 'Practical Object-Oriented Design in Ruby (POODR) is a programmers tale about how to write object-oriented code. It explains object-oriented design (OOD) using realistic, understandable examples. POODR will help you: - Decide what belongs in a single class - Avoid entangling objects that should be kept separate - Define flexible interfaces among objects - Reduce programming overhead costs with duck typing - Successfully apply inheritance - Build objects via composition - Design cost-effective tests - Craft simple, straightforward, understandable code',
    'proposer': 'paulina@typeform.com'
  },
  {
    'field_id': '21265069',
    'title': 'APIs: A Strategy Guide',
    'url': 'http://www.amazon.com/APIs-Strategy-Guide-Daniel-Jacobson/dp/1449308929',
    'the_proposer_have_read_it': true,
    'description_from_proposer': 'API strategy is going to be very important to Typeform for 2016, this book could provide our lead developers with a better understanding of what\'s involved.',
    'proposer': 'json@typeform.com'
  },
  {
    'field_id': '21265178',
    'title': 'RESTful Web APIs',
    'url': 'http://www.amazon.com/RESTful-Web-APIs-Leonard-Richardson/dp/1449358063',
    'the_proposer_have_read_it': true,
    'description_from_proposer': 'This is a seminal work in understanding the latest in API design, from two of the most influential authors in the space.',
    'proposer': 'json@typeform.com'
  },
  {
    'field_id': '21480918',
    'title': 'Docker in Action',
    'url': 'http://www.amazon.com/gp/product/1633430235/ref=s9_newrz_hd_bw_bzv_g14_i6',
    'the_proposer_have_read_it': false,
    'description_from_proposer': 'It covers almost everything we can need from docker as devs, and now that we are moving towards containerizing everything it may be useful for everyone to get the needed insight in the tool.',
    'proposer': 'marc@typeform.com'
  },
  {
    'field_id': '21480961',
    'title': 'Enterprise Integration Patterns: Designing, Building, and Deploying Messaging Solutions (Addison Wesley Signature Series)',
    'url': 'https://www.amazon.es/Enterprise-Integration-Patterns-Designing-Deploying/dp/0321200683',
    'the_proposer_have_read_it': true,
    'description_from_proposer': 'Nice explanations about different messaging patterns',
    'proposer': 'jorge@typeform.com'
  },
  {
    'field_id': '21481046',
    'title': 'Release It!: Design and Deploy Production-Ready Software',
    'url': 'http://www.amazon.es/Release-Production-Ready-Software-Pragmatic-Programmers-ebook/dp/B00A32NXZO',
    'the_proposer_have_read_it': true,
    'description_from_proposer': 'Provides a holistic view on how to release resilience and working software with some techniques like the usage of circuit breakers and bulkheads',
    'proposer': 'jorge@typeform.com'
  },
  {
    'field_id': '21481128',
    'title': 'Gamestorming: A Playbook for Innovators, Rulebreakers, and Changemakers',
    'url': 'http://www.amazon.com/Gamestorming-Playbook-Innovators-Rulebreakers-Changemakers/dp/0596804172',
    'the_proposer_have_read_it': false,
    'description_from_proposer': 'No one likes boring meetings, so could be interesting have some tools to bring fun and productivity to our meetings',
    'proposer': 'jorge@typeform.com'
  },
  {
    'field_id': '21481174',
    'title': 'Extreme Programming Explained: Embrace Change, 2nd Edition',
    'url': 'http://www.amazon.com/Extreme-Programming-Explained-Embrace-Edition/dp/0321278658',
    'the_proposer_have_read_it': true,
    'description_from_proposer': 'XP provides a good set of good practices, values and principles to produce good software continuously',
    'proposer': 'jorge@typeform.com'
  },
  {
    'field_id': '21481194',
    'title': 'The Zen Programmer',
    'url': 'http://www.amazon.es/Zen-Programmer-English-Christian-Grobmeier-ebook/dp/B00WG5XLE4',
    'the_proposer_have_read_it': false,
    'description_from_proposer': 'It\'s an exploration about how to live a balanced, sustainable life as a programmer. Could help us work smarter and be more humble. :)',
    'proposer': 'nabeelah@typeform.com'
  }
]

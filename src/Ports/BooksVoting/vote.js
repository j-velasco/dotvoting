import UserId from 'Domain/Model/UserId'
import VoteService from 'Application/VoteService'

export default function vote (bookVotingId, data, usersRepository, db, votingRepository, callback) {
  try {
    const voteService = new VoteService(votingRepository, usersRepository)
    const dotsPerAlternative = data.form_response.answers.map((a) => {
      return {
        alternative: a.field.id,
        dots: a.number
      }
    })
    const id = new UserId(data.form_response.hidden.token)
    const user = usersRepository.userOfId(id)

    voteService.voteInVoting(bookVotingId, '' + user.email, dotsPerAlternative)

    votingRepository.votings.forEach((voting) => {
      db.update('aggregate', { data: voting.dto }).where({
        type: 'Domain/Model/Voting',
        id: voting.dto.id
      }).run((err) => {
        callback(err)
      })
    })
  } catch (err) {
    callback(err)
  }
}

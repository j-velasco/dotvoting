import express from 'express'
import path from 'path'
import bodyParser from 'body-parser'

import createUserRepository from './createUserRepository'
import createVotingRepository from './createVotingRepository'
import GetVotingServiceById from 'Application/GetVotingServiceById'
import UserId from 'Domain/Model/UserId'
import vote from './vote'
import createResultVoting from 'Application/createResultVoting'
import books from './data/suggested_books'

import db from './db'
const app = express()

const votingRepository = createVotingRepository(db)
const usersRepository = createUserRepository(db)
const getVotingById = new GetVotingServiceById(votingRepository)
const getBookOfId = (id) => {
  return books.filter((book) => book.field_id === id)[ 0 ]
}

const votingResultService = createResultVoting(votingRepository)

app.use(bodyParser.json())

app.post('/voting/:id/vote', (req, res) => {
  const payload = req.body
  const searchWebhookQuery = db.select().from('aggregate').where({
    type: 'webhook',
    id: payload.event_id
  })
  searchWebhookQuery.run((err, result) => {
    if (err) {
      console.error('error searching webhook', err)
      res.sendStatus(500)
      return
    }
    if (result.rowCount >= 1) {
      console.info(`webhook "${payload.event_id}" already processed`)
      res.sendStatus(200)
      return
    }

    db.run((client) => {
      client.insert('aggregate', {
        type: 'webhook',
        id: payload.event_id,
        data: payload
      }).run((err) => {
        console.log('webhook saved')
        if (err) {
          console.error(err)
        }
      })

      const bookVotingId = req.params.id
      vote(bookVotingId, payload, usersRepository, client, votingRepository, (err) => {
        console.info('vote saved', payload)
        if (err) {
          res.sendStatus(500)
        } else {
          res.sendStatus(200)
        }
      })
    })
  })
})

app.use('/', express.static(path.join(__dirname, '../../..', '/node_modules/milligram/dist/')))

app.get('/voting/:id/result', (req, res) => {
  const votingId = req.params.id
  const resultDto = votingResultService(votingId)
  const rating = resultDto.map((line) => {
    return {
      book: getBookOfId(line.alternative),
      dots: line.dots,
      position: line.position
    }
  })

  const asHtmlRows = (rating) => {
    return rating.map((line) => {
      return `<tr class="row ${line.position <= 5 ? 'selected' : ''}">
                  <td class="rating column column-10">${line.position}</td>
                  <td class="dots column column-10">${line.dots}</td>
                  <td class="book column column-70">
                    <a href="${line.book.url}">${line.book.title}</a>
                  </td>
              </tr>`
    }).join('')
  }

  res.send(
    `<html>
        <head>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.css">
            <link rel="stylesheet" href="/milligram.min.css">
            <style>
                body { background-color: rgba(300, 300, 300, 0.2); }
                .selected { background-color: rgba(80, 255, 50, 0.5) }
                th { line-height: 40px;     background: rgba(200, 200, 200, 0.2); text-align: center; }
                td { line-height: 30px; }
                .rating, .dots { text-align: center; font-weight: 600 }
            </style>
        </head>
        <body>
            <table class="container">
                <thead>
                    <tr class="row">
                      <th class="rating column column-10">Ranking</th>
                      <th class="dots column column-10">Dots</th>
                      <th class="book column column-70">Book</th>
                    </tr>
                </thead>
                <tbody>
                    ${asHtmlRows(rating)}
                </tbody>
            </table>
        </body>
    </html>`
  )
})

app.get('/voting/:id/votes', (req, res) => {
  const votingId = req.params.id
  const votingDto = getVotingById.votingWithId(votingId)
  res.json(votingDto.votes.map((vote) => {
    const voter = usersRepository.userOfId(new UserId(vote.voterId))
    return {
      voter: '' + voter.email,
      dotsPerAlternative: vote.dotsPerAlternative.map((dotPerAlternative) => {
        return {
          book: getBookOfId(dotPerAlternative.alternative).title,
          dots: dotPerAlternative.dots
        }
      })
    }
  }))
})

const port = process.env.PORT || 8000
app.listen(port, () => {
  console.log(`✅  The server is running at http://localhost:${port}/`)
})

import UserRepositoryBase from 'Domain/Model/UserRepository'

class UserRepository extends UserRepositoryBase {
  constructor () {
    super()
    this.users = []
  }

  save (anUser) {
    this.users.push(anUser)
  }

  userOfId (anId) {
    const searchResult = this.users
      .filter((u) => u.id.equalsTo(anId))

    if (searchResult.length === 0) {
      return null
    }

    return searchResult[0]
  }
}

module.exports = UserRepository

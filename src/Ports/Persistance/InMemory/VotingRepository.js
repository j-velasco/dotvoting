import { default as BaseVotingRepository } from 'Domain/Model/VotingRepository'

class VotingRepository extends BaseVotingRepository {
  constructor () {
    super()
    this.votings = new Map()
  }

  votingOfId (anId) {
    return this.votings.get(anId.id)
  }

  save (aVoting) {
    this.votings.set(aVoting.id.id, aVoting)
  }
}

export default VotingRepository

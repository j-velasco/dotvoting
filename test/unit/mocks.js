import Voting from '../../src/Domain/Model/Voting'
import Result from '../../src/Domain/Model/Result'

const givenVotingExistsInRepo = (aVotingRepository, aVotingId) => {
  const aVotingResultDto = ['pos1', 'pos2']
  const aVoting = mock(Voting)
  const aVotingResult = mock(Result)
  when(aVotingResult).dto().thenReturn(aVotingResultDto)
  when(aVoting).result().thenReturn(aVotingResult)

  when(aVotingRepository)
    .votingOfId(withVotingId(aVotingId))
    .thenReturn(aVoting)

  return {
    aVoting, aVotingResult, aVotingResultDto
  }
}



export {
  givenVotingExistsInRepo
}

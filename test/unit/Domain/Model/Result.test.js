import Alternative from 'Domain/Model/Alternative'
import VotingId from 'Domain/Model/VotingId'
import UserId from 'Domain/Model/UserId'
import Vote from 'Domain/Model/Vote'
import Result from 'Domain/Model/Result'

describe('Result entity', () => {
  describe('toDto', () => {
    it('return empty for empty alternatives and votes', () => {
      const result = new Result([], [])
      assertThat(result.dto(), equalTo([]))
    })

    it('return alternatives in the given order when no votes', () => {
      const anAlternative = new Alternative('a')
      const otherAlternative = new Alternative('b')
      const expectedDto = [ {
        position: 1,
        alternative: 'a',
        dots: 0
      }, {
        position: 1,
        alternative: 'b',
        dots: 0
      } ]

      const result = new Result([ anAlternative, otherAlternative ], [])

      assertThat(result.dto(), equivalentArray(expectedDto))
    })

    it('counts draw positions', () => {
      const anAlternativeWithOneDot = new Alternative('a')
      const otherAlternativeWithOneDot = new Alternative('b')
      const anAlternativeWithoutAnyDot = new Alternative('c')

      const aListOfVotesPerAlternative = [
        {
          alternative: anAlternativeWithOneDot,
          dots: 1
        },
        {
          alternative: otherAlternativeWithOneDot,
          dots: 1
        }
      ]
      const aUserVote = new Vote(new VotingId(), new UserId(), aListOfVotesPerAlternative)

      const result = new Result(
        [ anAlternativeWithOneDot, otherAlternativeWithOneDot, anAlternativeWithoutAnyDot ],
        [ aUserVote ]
      )

      const expectedDto = [
        {
          position: 1,
          alternative: 'a',
          dots: 1
        },
        {
          position: 1,
          alternative: 'b',
          dots: 1
        },
        {
          position: 3,
          alternative: 'c',
          dots: 0
        }
      ]

      assertThat(result.dto(), equivalentArray(expectedDto))
    })

    it('count only the last vote of a user', () => {
      const anAlternative = new Alternative('a')
      const anUserId = new UserId()

      const listForFirstVote = [
        {
          alternative: anAlternative,
          dots: 1
        }
      ]
      const firstUserVote = new Vote(new VotingId(), anUserId, listForFirstVote)

      const listForSecondVote = [
        {
          alternative: anAlternative,
          dots: 2
        }
      ]
      const secondUserVote = new Vote(new VotingId(), anUserId, listForSecondVote)

      const result = new Result([anAlternative], [firstUserVote, secondUserVote])

      const expectedDto = [{
        position: 1,
        alternative: 'a',
        dots: 2
      }]
      assertThat(result.dto(), equivalentArray(expectedDto))
    })
  })
})

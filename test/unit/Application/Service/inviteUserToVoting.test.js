import { factory } from '../../../../src/Application/Service/inviteUserToVoting'
import VotingRepository from '../../../../src/Domain/Model/VotingRepository'
import UnknownVoting from '../../../../src/Application/Service/UnknownVoting'
import { givenVotingExistsInRepo } from '../../mocks'

describe('invite user to voting service', () => {
  describe('its factory', () => {
    it('returns the service as a function', () => {
      assertThat(factory(), is(func()))
    })
  })

  describe('the service its self', () => {
    const aVotingId = 'voting-1234'
    const aUserId = 'user-1234'
    let aVotingRepository
    let inviteUserToVotingService

    beforeEach(() => {
      aVotingRepository = mock(VotingRepository)
      inviteUserToVotingService = factory(aVotingRepository)
    })

    it('fetch the voting from voting repository', () => {
      givenVotingExistsInRepo(aVotingRepository, aVotingId)

      inviteUserToVotingService(aVotingId)

      verify(aVotingRepository).votingOfId(withVotingId(aVotingId))
    })

    it('throw error when voting does not exists', () => {
      let ex
      try {
        inviteUserToVotingService(aVotingId)
      } catch (e) {
        ex = e
      }

      assertThat(ex, unknownVotingWithId(aVotingId))
    })

    it('add voter to voting', () => {
      const {aVoting} = givenVotingExistsInRepo(aVotingRepository, aVotingId)

      inviteUserToVotingService(aVotingId, aUserId)

      verify(aVoting).addVoter(withUserId(aUserId))
    })
  })
})

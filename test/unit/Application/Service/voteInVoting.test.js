import {factory} from '../../../../src/Application/Service/voteInVoting'
import VotingRepository from '../../../../src/Domain/Model/VotingRepository'
import { givenVotingExistsInRepo } from '../../mocks'

describe('vote in voting service', () => {
  describe('its factory', () => {
    it('returns the service as a function', () => {
      const service = factory();
      assertThat(service, is(func()))
    })
  })

  describe('the service', () => {
    const aVotingId = 'voting-1234'
    const aDotsPerAlternativeList = []
    const anUserId = 'user-5678'

    let aVotingRepository
    let service

    beforeEach(() => {
      aVotingRepository = mock(VotingRepository)
      service = factory(aVotingRepository)
    })

    it('fetch the voting from voting repository', () => {
      givenVotingExistsInRepo(aVotingRepository, aVotingId)

      service(aVotingId, anUserId, aDotsPerAlternativeList)

      verify(aVotingRepository).votingOfId(withVotingId(aVotingId))
    })

    it('throw error when voting does not exists', () => {
      let ex
      try {
        service(aVotingId, anUserId, aDotsPerAlternativeList)
      } catch (e) {
        ex = e
      }

      assertThat(ex, unknownVotingWithId(aVotingId))
    })

    it('adds vote to voting', () => {
      const { aVoting } = givenVotingExistsInRepo(aVotingRepository, aVotingId)

      service(aVotingId, anUserId, aDotsPerAlternativeList)

      verify(aVoting)
        .addVote(withUserId(anUserId), aDotsPerAlternativeList)
    })
  })
})

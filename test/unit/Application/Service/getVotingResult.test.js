import {factory} from '../../../../src/Application/Service/getVotingResult'
import VotingRepository from '../../../../src/Domain/Model/VotingRepository'
import UnknownVoting from '../../../../src/Application/Service/UnknownVoting'
import {givenVotingExistsInRepo} from '../../mocks'

describe('get voting\'s results', () => {
  describe('its factory', () => {
    it('returns the service as a function', () => {
      assertThat(factory(), is(func()))
    })
  })

  describe('the service it self', () => {
    const aVotingId = 'voting-1234'

    let aVotingRepository
    let service

    beforeEach(() => {
      aVotingRepository = mock(VotingRepository)
      service = factory(aVotingRepository)
    })

    it('fetch the voting from the repository', () => {
      givenVotingExistsInRepo(aVotingRepository, aVotingId)
      service(aVotingId)

      verify(aVotingRepository)
        .votingOfId(withVotingId(aVotingId))
    })

    it('throws error when voting does not exists', () => {
      let ex
      try {
        service(aVotingId)
      } catch (e) {
        ex = e
      }

      assertThat(ex, is(instanceOf(UnknownVoting)))
    })

    it('return result dto', () => {
      const {aVotingResultDto} = givenVotingExistsInRepo(aVotingRepository, aVotingId)

      const actualResult = service(aVotingId)

      assertThat(actualResult, is(aVotingResultDto))
    })
  })
})

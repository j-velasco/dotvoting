import VotingId from "../src/Domain/Model/VotingId"
import UserId from "../src/Domain/Model/UserId";
import UnknownVoting from '../src/Application/Service/UnknownVoting'
import UserNotAllowedToVote from "../src/Application/Service/UserNotAllowedToVote";

const withVotingId = (anExpectedId) => {
  const expectedId = new VotingId(anExpectedId)
  return new JsHamcrest.SimpleMatcher({
    matches: (actual) => {
      return expectedId.equalsTo(actual)
    },
    describeTo: (description) => {
      description.append('the answer to life, the universe, and everything')
    }
  })
}

const withUserId = (anUserId) => {
  const expectedId = new UserId(anUserId)
  return new JsHamcrest.SimpleMatcher({
    matches: (otherId) => {
      return expectedId.equalsTo(otherId)
    },
    describeTo: (description) => {
      description.append(`UserId entity with value ${expectedId}`)
    }
  })
}

const unknownVotingWithId = (expectedId) => {
  const expectedError = new UnknownVoting(expectedId)
  return new JsHamcrest.SimpleMatcher({
    matches: (ex) => {
      return ex instanceof UnknownVoting && ex.message === expectedError.message
    },
    describeTo: (description) => {
      description.append(`UnknownVoting error with message ${expectedError.message}`)
    }
  })
}

const userNotAllowedToVoteOnVoting = (votingId, userId) => {
  const expectedError = new UserNotAllowedToVote(new VotingId(votingId), new UserId(userId))
  return new JsHamcrest.SimpleMatcher({
    matches: (ex) => {
      console.log('userNotAllowedToVoteOnVoting', ex.message, expectedError.message)
      return ex instanceof UserNotAllowedToVote && ex.message === expectedError.message
    },
    describeTo: (description) => {
      description.append(`UserNotAllowedToVote error with message ${expectedError.message}`)
    }
  })
}

export { withVotingId, withUserId, unknownVotingWithId, userNotAllowedToVoteOnVoting }

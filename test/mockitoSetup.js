const JsMockito = require('jsmockito').JsMockito
const matchers = require('./matchers')

global.JsHamcrest.Integration.installMatchers(matchers)


global.JsHamcrest.Integration.copyMembers(global)
JsMockito.Integration.importTo(global)

// TODO open PR for this integration on JsMockito repo
global.assertThat = (actual, matcher, message, done) => {
  return global.JsHamcrest.Operators.assert(actual, matcher, {
    message: message,
    fail: function (message) {
      throw new Error(message)
    },
    pass: function () {
      if (done) {
        done()
      }
    }
  })
}

global.mock = (obj, delegate) => {
  const extractInterface = () => {
    const propertiesToCheck = Object.getOwnPropertyNames(obj).concat(Object.getOwnPropertyNames(obj.prototype))
    const functions = propertiesToCheck.filter((prop) => typeof (obj[prop] || obj.prototype[prop]) === 'function');
    return functions.reduce((m, methodName) => {
      m[methodName] = () => null
      return m
    }, {})
  }

  return JsMockito.mock(extractInterface(), delegate)
}

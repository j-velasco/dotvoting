'use strict'

var path = require('path')
var appModulePath = require('app-module-path')
appModulePath.addPath(path.join(__dirname, '../src'))

require('./mockitoSetup')